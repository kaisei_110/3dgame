using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoodManController : MonoBehaviour
{
    GameObject player;
    GameObject director;
    void Start()
    {
        player = GameObject.Find("Player");
        this.director = GameObject.Find("GameDirector");
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(-0.01f,0, 0);

        if(transform.position.x < -10f)
        {
            Destroy(gameObject);
        }

        Vector3 p1 = transform.position;
        Vector3 p2 = player.transform.position;
        Vector3 dir = p1 - p2;
        float d =dir.magnitude;
        float r1 = 0.5f;
        float r2 = 1.0f;


        if (d < r1 + r2)
        {
            Destroy(gameObject);
        }
    }

    void OntriggerEnter(Collider other)
    {
        this.director.GetComponent<GameDirector>().GatGoodMan();
    }

}
