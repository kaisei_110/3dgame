using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BadManGenerator : MonoBehaviour
{
    public GameObject BadManPrefab;
    float span = 1.0f;
    float delta = 0;


    void Update()
    {
        this.delta += Time.deltaTime;
        if (this.delta > this.span)
        {
            this.delta = 0;
            GameObject go = Instantiate(BadManPrefab) as GameObject;
            int px = Random.Range(-7, 7);
            go.transform.position = new Vector3(15, 0, px);
        }

    }

}
